#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2012  IPFire Development Team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

BASEDIR="/usr/share/ipfire-dit"
TEMPLATE="${BASEDIR}/template.ldif"

DOMAIN=${HOSTNAME#*.}
PASSWORD=

function parse_cli() {
	while [ $# -gt 0 ]; do
		case "${1}" in
			-p)
				PASSWORD=${2}
				shift
				;;
			*)
				DOMAIN=${1}
				;;
		esac
		shift
	done

	while [ -z "${PASSWORD}" ]; do
		echo -n "Enter password: "
		read -s PASSWORD
		echo
	done

	# Check for valid input data.

	if [ ${#PASSWORD} -lt 8 ]; then
		echo "Using weak password. Must at least have 8 characters!" >&2
		exit 1
	fi

	if [ -z "${DOMAIN}" ]; then
		echo "Domain is empty." >&2
		exit 1
	fi

	# XXX check domain for invalid characters
}

function substitude_ldif() {
	local output=${1}

	local suffix bit dc
	for bit in ${DOMAIN//./ }; do
		if [ -n "${suffix}" ]; then
			suffix="${suffix},dc=${bit}"
		else
			dc="${bit}"
			suffix="dc=${bit}"
		fi
	done

	sed \
		-e "s/@DC@/${dc}/g" \
		-e "s/@SUFFIX@/${suffix}/g" \
		< ${TEMPLATE} > ${output}
}

function load_database() {
	local new_ldif=$(mktemp)
	trap "rm -f ${new_ldif}" EXIT KILL TERM

	substitude_ldif ${new_ldif}

	slapadd < ${new_ldif}
}

function generate_pwhash() {
	local password="${PASSWORD}"

	slappasswd -h "{SSHA}" -s "${password}"
}

# Hello to this wonderful script.
# Firstly, let's see what we need to do.
parse_cli $@

# Now, we got all the information we need, we
# can load the database.
load_database
